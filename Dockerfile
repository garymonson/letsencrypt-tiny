FROM alpine:3.3
MAINTAINER gary.monson@gmail.com

RUN apk update
RUN apk add python
RUN apk add openssl
RUN apk add ca-certificates

RUN wget -O /tmp/acme-tiny.zip https://github.com/diafygi/acme-tiny/archive/7a5a2558c8d6e5ab2a59b9fec9633d9e63127971.zip
RUN mkdir /opt
RUN (cd /opt; unzip /tmp/acme-tiny.zip)
RUN ln -s /opt/acme-tiny-7a5a2558c8d6e5ab2a59b9fec9633d9e63127971 /opt/acme-tiny

COPY update-certs /update-certs

VOLUME /acme-challenge

CMD /update-certs
