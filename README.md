# LetsEncrypt-Tiny

A docker image to manage SSL certificates using the Lets-Encrypt certificate
authority.

Built from the Alpine docker image, and using acme-tiny for the lets-encrypt
client.

## Building the image

```
docker build -t letsencrypt-tiny:0.0.1 .
```

## How to use

For now the container runs the update script once then exits.  It also does not
yet reconfigure or restart the webserver container.

You must have a Let's Encrypt account key, and provide it to the container in
/etc/secrets/account.key.  If you do not have one yet, generate it as follows:

```
openssl genrsa 4096 > account.key
```

You must also provide a domain private key for each domain to be managed, at
/etc/secrets/$domain.key.  To generate such a key:

```
openssl genrsa 4096 > $domain.key
```

Then you can run the container, providing the key files in /etc/secrets, and
existing .pem certificates in /certs:

```
docker run -it --rm -v /path/to/secrets:/etc/secrets -v /path/to/certs:/certs -e DOMAINS="$DOMAINS" --name letsencrypt letsencrypt-tiny:0.0.1
```

where $DOMAINS is space separated list of domains.

Example:

```
docker run -it --rm -v $PWD/secrets:/etc/secrets -v $PWD/certs:/certs -e DOMAINS="www1.example.com www2.example.com" --name letsencrypt letsencrypt-tiny:0.0.1
```

The container will check each (chained) domain certificate to see if it will
expire within the next 28 days.  If the certificate is missing, already
expired, or good for less than 28 days, then it will try to generate a new
certificate from Let's Encrypt.  On success, it will then chain the signed
certificate with the Let's Encrypt intermediate certificate, and save to
/certs.

The DNS records for the DOMAINS must resolve correctly for port 80, to a
webserver configured for path /.well-known/acme-challenge to be served from the
volume /acme-challenge in this container.

The web server container should be run with the keys, certificates and acme
challenges made available via mounts.  For example:

```
docker run -it --rm -v $PWD/certs:/etc/nginx/certs -v $PWD/secrets:/etc/secrets --link letsencrypt:letsencrypt -p 80:80 -p 443:443 --volumes-from letsencrypt website-image
```

You also need to configure the web server to use the /acme-challenge volume
from the letsencrypt container:

```
server {
    listen 80;
    server_name localhost;

    location /.well-known/acme-challenge/ {
        alias /acme-challenge/;
    }

    location / {
      return 301 https://$host$request_uri;
    }
}
```

Then your SSL virtualhost can be configured with the key file and cert file:

```
server {
    listen 443 ssl;
    server_name localhost;

    ssl_certificate /etc/nginx/certs/www1.example.com-chained.pem;
    ssl_certificate_key /etc/secrets/www1.example.com.key;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
    ssl_session_timeout 1d;
    ssl_session_cache shared:SSL:50m;
    ssl_stapling on;
    ssl_stapling_verify on;
    add_header Strict-Transport-Security max-age=15768000;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }
}
```

## License

MIT for this docker configuration, the components are licensed according to
their own terms.
